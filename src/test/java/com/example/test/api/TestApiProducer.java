package com.example.test.api;

import com.example.test.api.model.Producer;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@SpringBootTest
@AutoConfigureMockMvc
class TestApiProducer {

    @Autowired
    private MockMvc mockMvc;

    @Test
    public void getAllProducersApi() throws Exception {
        this.mockMvc.perform(get("/api/producers")).
                andDo(print()).andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON)).
                andExpect(MockMvcResultMatchers.jsonPath("$[0].id").exists()).
                andExpect(MockMvcResultMatchers.jsonPath("$[0].inn").exists());
    }

    @Test
    public void saveProducersApi() throws Exception {
        Producer producer = new Producer();
        producer.setId(null);
        producer.setInn("1292");
        producer.setKpp("1293");
        producer.setFullName("Общество с ограниченной ответственностью Тест save");
        producer.setShortName("ООО Тест save2");
        mockMvc.perform(post("/api/producers")
                .content(asJsonString(producer))
                .contentType(MediaType.APPLICATION_JSON)
                .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(MockMvcResultMatchers.jsonPath("$.id").exists())
                .andExpect(MockMvcResultMatchers.jsonPath("$.inn").exists());
    }

    @Test
    public void putProducerApi() throws Exception {
        Producer producer = new Producer();
        producer.setId(1);
        producer.setInn("1293");
        producer.setKpp("1294");
        producer.setFullName("Общество с ограниченной ответственностью Тест change");
        producer.setShortName("ООО Тест change");
        mockMvc.perform(MockMvcRequestBuilders
                .put("/api/producers/1", 2)
                .content(asJsonString(producer))
                .contentType(MediaType.APPLICATION_JSON)
                .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(MockMvcResultMatchers.jsonPath("$.inn").value("1293"))
                .andExpect(MockMvcResultMatchers.jsonPath("$.kpp").value("1294"))
                .andExpect(MockMvcResultMatchers.jsonPath("$.fullName").value("Общество с ограниченной ответственностью Тест change"))
                .andExpect(MockMvcResultMatchers.jsonPath("$.shortName").value("ООО Тест change"));
    }

    @Test
    public void deleteProducerApi() throws Exception {
        Producer producer = new Producer();
        producer.setId(2);
        producer.setInn("222");
        producer.setKpp("333");
        producer.setFullName("Общество с ограниченной ответственностью Тест del");
        producer.setShortName("ООО Тест del");
        mockMvc.perform(MockMvcRequestBuilders.delete("/api/producers/1", producer).
                content(asJsonString(producer)).contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isAccepted());
    }

    private static String asJsonString(final Object obj) {
        try {
            return new ObjectMapper().writeValueAsString(obj);
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }
}
