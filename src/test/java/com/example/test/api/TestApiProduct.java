package com.example.test.api;

import com.example.test.api.model.Producer;
import com.example.test.api.model.Product;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@SpringBootTest
@AutoConfigureMockMvc
public class TestApiProduct {

    @Autowired
    private MockMvc mockMvc;

    @Test
    public void getAllProductApi() throws Exception {
        this.mockMvc.perform(get("/api/products")).
                andDo(print()).andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON)).
                andExpect(MockMvcResultMatchers.jsonPath("$[0].id").exists()).
                andExpect(MockMvcResultMatchers.jsonPath("$[0].code").exists());
    }

    @Test
    public void saveProductApi() throws Exception {
        Product product = new Product();
        product.setId(null);
        product.setCode("1293");
        product.setOriginalName("Продукт2");
        Producer producer = new Producer();
        producer.setId(2);
        product.setProducer(producer);
        mockMvc.perform(post("/api/products")
                .content(asJsonString(product))
                .contentType(MediaType.APPLICATION_JSON)
                .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(MockMvcResultMatchers.jsonPath("$.id").exists());
    }

    @Test
    public void putProducerApi() throws Exception {
        Product product = new Product();
        product.setId(1);
        product.setCode("1222");
        product.setOriginalName("Продукт3");
        Producer producer = new Producer();
        producer.setId(2);
        product.setProducer(producer);
        mockMvc.perform(MockMvcRequestBuilders
                .put("/api/products/2", 2)
                .content(asJsonString(product))
                .contentType(MediaType.APPLICATION_JSON)
                .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(MockMvcResultMatchers.jsonPath("$.id").value("1"))
                .andExpect(MockMvcResultMatchers.jsonPath("$.code").value("1222"))
                .andExpect(MockMvcResultMatchers.jsonPath("$.originalName").
                        value("Продукт3"));
    }

    @Test
    public void deleteProducerApi() throws Exception {
        Product product = new Product();
        product.setId(1);
        product.setCode("1293");
        product.setOriginalName("Продукт2");
        Producer producer = new Producer();
        producer.setId(1);
        product.setProducer(producer);
        mockMvc.perform(MockMvcRequestBuilders.delete("/api/producers/2", producer).
                content(asJsonString(producer)).contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isAccepted());
    }

    private static String asJsonString(final Object obj) {
        try {
            return new ObjectMapper().writeValueAsString(obj);
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }
}
