package com.example.test.api.controller;

import com.example.test.api.model.Product;
import com.example.test.api.repository.ProductRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
public class ProductController {

    private final ProductRepository repository;

    @Autowired
    public ProductController(ProductRepository repository) {
        this.repository = repository;
    }

    @GetMapping("/api/products")
    public Iterable<Product> getProducts() {
        return repository.findAll();
    }

    @PostMapping("/api/products")
    @ResponseBody
    public Product saveProduct(@RequestBody Product product) {
        System.out.println(product);
        return repository.save(product);
    }

    @PutMapping("/api/products/2")
    @ResponseBody
    public Product updateProduct(@RequestBody Product product) {
        return repository.save(product);
    }

    @DeleteMapping("/api/products/2")
    public ResponseEntity<HttpStatus> delProduct(@RequestBody Product product) {
        repository.delete(product);
        return new ResponseEntity<>(HttpStatus.ACCEPTED);
    }
}
