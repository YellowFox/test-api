package com.example.test.api.controller;

import com.example.test.api.model.Producer;
import com.example.test.api.repository.ProducerRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
public class ProducerController {

    private final ProducerRepository repository;

    @Autowired
    public ProducerController(ProducerRepository repository) {
        this.repository = repository;
    }

    @GetMapping("/api/producers")
    public Iterable<Producer> getProducer() {
        return repository.findAll();
    }

    @PostMapping(value = "/api/producers", produces = "application/json")
    @ResponseBody
    public Producer saveProducer(@RequestBody Producer producer) {
        return repository.save(producer);
    }

    @PutMapping(value = "/api/producers/1", produces = "application/json")
    @ResponseBody
    public Producer updateProducer(@RequestBody Producer producer) {
        return repository.save(producer);
    }

    @DeleteMapping("/api/producers/1")
    public ResponseEntity<HttpStatus> delProducer(@RequestBody Producer producer) {
        repository.delete(producer);
        return new ResponseEntity<>(HttpStatus.ACCEPTED);
    }
}
