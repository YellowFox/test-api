package com.example.test.api.model;

import javax.persistence.*;
import javax.validation.constraints.NotBlank;

@Entity
public class Producer {

    @Id
    @GeneratedValue(strategy= GenerationType.IDENTITY)
    private Integer id;

    @Column(nullable = false, unique = true)
    @NotBlank
    private String inn;

    @Column(nullable = false)
    @NotBlank
    private String kpp;

    @Column(nullable = false)
    @NotBlank
    private String fullName;

    @Column(nullable = false)
    @NotBlank
    private String shortName;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getInn() {
        return inn;
    }

    public void setInn(String inn) {
        this.inn = inn;
    }

    public String getKpp() {
        return kpp;
    }

    public void setKpp(String kpp) {
        this.kpp = kpp;
    }

    public String getFullName() {
        return fullName;
    }

    public void setFullName(String fullName) {
        this.fullName = fullName;
    }

    public String getShortName() {
        return shortName;
    }

    public void setShortName(String shortName) {
        this.shortName = shortName;
    }

    @Override
    public String toString() {
        return "Producer{" +
                "id=" + id +
                ", inn='" + inn + '\'' +
                ", kpp='" + kpp + '\'' +
                ", fullName='" + fullName + '\'' +
                ", shortName='" + shortName + '\'' +
                '}';
    }
}
